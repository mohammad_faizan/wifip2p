package com.example.wifip2p;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.WifiP2pManager.Channel;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnItemClickListener  {

	private WifiP2pManager mManager;
	private Channel mChannel;
	private BroadcastReceiver mReceiver;
	private IntentFilter filter;
	
	private ListView mDevieList;
	private List<WifiP2pDevice> mAvailablePeers = new ArrayList<WifiP2pDevice>();
	
	private ArrayAdapter<String> mAdapter;
	
//	private WifiManager wifiManager;
	
//	public static boolean wifiFlag = false;
	
	public static String TAG = "Wifi P2P";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

//		wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
//		if(!wifiManager.isWifiEnabled()){
//			wifiManager.setWifiEnabled(true);
//			wifiFlag = true;
//		}
		
//		while(!wifiManager.isWifiEnabled()){
//			try {
//				Thread.sleep(200);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
//		}
		
		mManager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
		
		if(mManager == null){
			Toast.makeText(this, "Wifi P2p not available", Toast.LENGTH_SHORT).show();
			finish();
		}

		mChannel = mManager.initialize(this, getMainLooper(), null);
				
		mDevieList = (ListView) findViewById(R.id.device_list);
		mAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
		mDevieList.setAdapter(mAdapter);
		mDevieList.setOnItemClickListener(this);
		
		filter = new IntentFilter();
		filter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
		filter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
		filter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
		filter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);
		
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		mReceiver = new WifiBroadcastReciever(mManager, mChannel, this);
		registerReceiver(mReceiver, filter);

		mManager.discoverPeers(mChannel, new WifiP2pManager.ActionListener() {
			
			@Override
			public void onSuccess() {
				Toast.makeText(getApplicationContext(), "Discover peers success", Toast.LENGTH_SHORT).show();
				Log.d("Discovery initated", "Success");
			}
			
			@Override
			public void onFailure(int reason) {
				Log.d("Discovery initated", "Failure");
				Toast.makeText(getApplicationContext(), "Discover peers failure", Toast.LENGTH_SHORT).show();
			}
		});
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		unregisterReceiver(mReceiver);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
//		if(wifiFlag)
//			wifiManager.setWifiEnabled(false);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void updateList(WifiP2pDeviceList peers) {
		Log.d(TAG, "Peers available callback");
		mAvailablePeers.clear();
		for(WifiP2pDevice device : peers.getDeviceList()){
			mAvailablePeers.add(device);
			mAdapter.add(device.deviceName + "\n" + device.deviceAddress);
		}
		mAdapter.notifyDataSetChanged();
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		
	}

}
