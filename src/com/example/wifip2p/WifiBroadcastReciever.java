package com.example.wifip2p;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.WifiP2pManager.Channel;
import android.net.wifi.p2p.WifiP2pManager.PeerListListener;
import android.util.Log;

public class WifiBroadcastReciever extends BroadcastReceiver {
	
	private WifiP2pManager mManager;
	private Channel mChannel;
	private MainActivity mActivity;
	
	public WifiBroadcastReciever(WifiP2pManager mManager, Channel mChannel, MainActivity mActivity){
		super();
		this.mManager = mManager;
		this.mChannel = mChannel;
		this.mActivity = mActivity;
	}

	@Override
	public void onReceive(Context c, Intent intent) {
		String action = intent.getAction();
		
		if(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)){
			int state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1);
			
			Log.d(MainActivity.TAG, "Wifi p2p state changed with state : " + state);
			
			if(state == WifiP2pManager.WIFI_P2P_STATE_ENABLED){
				Log.d(MainActivity.TAG, "Wifi direct enabled");
			}else{
				Log.d(MainActivity.TAG, "Wifi direct not enabled");
			}
			
		}else if(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION.equals(action)){
			Log.d(MainActivity.TAG, "Wifi p2p peers changed");
			
			if(mManager != null){
				Log.d(MainActivity.TAG, "peers requested");
				mManager.requestPeers(mChannel, new PeerListListener() {
					
					@Override
					public void onPeersAvailable(WifiP2pDeviceList peers) {
						mActivity.updateList(peers);
					}
				});
			}
			
		}else if(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action)){
			Log.d(MainActivity.TAG, "Wifi p2p connection changed");
			
		}else if(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION.equals(action)){
			Log.d(MainActivity.TAG, "Wifi p2p this device changed");
		}

	}

}
